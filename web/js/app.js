var app = angular.module('app', ['ngAnimate', 'ngSanitize', 'ui.bootstrap']);

app.controller('AppController', ['$scope', '$http', AppController]);

app.directive('file', function() {
    return {
        require:"ngModel",
        restrict: 'A',
        link: function($scope, el, attrs, ngModel){
            el.bind('change', function(event){
                var files = event.target.files;
                var file = files[0];
                ngModel.$setViewValue(file);
                $scope.$apply();
            });
        }
    };
});

function AppController($scope, $http) {
    $scope.authorAddLock = false;
    $scope.bookAddLock = false;

    $scope.unlockAddAuthor = function () {
        $scope.authorAddLock = false;
    };

    $scope.unlockAddBook = function () {
        $scope.bookAddLock = false;
    };


    $scope.getAuthors = function () {
        $http.get('authors').then(function (response) {
            $scope.authors = response.data;
        });
    };

    $scope.getBooks = function () {
        $http.get('books').then(function (response) {
            $scope.books = response.data;
        });
    };

    $scope.newAuthor = function () {
        $scope.authorAddLock = true;
        var newAuthor = {
            'id': null,
            'firstName': '',
            'lastName': '',
            'middleName': '',
            'books': [],
            '__toString': '',
            'changed': true
        };
        $scope.authors.unshift(newAuthor);
    };

    $scope.newBook = function () {
        $scope.bookAddLock = true;
        var newBook = {
            'id': null,
            'name': '',
            'year': null,
            'isbn': '',
            'pagesNumber': null,
            'authors': [],
            '__toString': '',
            'changed': true
        };
        $scope.books.unshift(newBook);
    };

    $scope.newAuthorBook = function (index) {
        var newBook = {
            'id': null,
            '__toString': ''
        };
        $scope.authors[index].books.unshift(newBook);
    };

    $scope.newBookAuthor = function (index) {
        var newAuthor = {
            'id': null,
            '__toString': ''
        };
        $scope.books[index].authors.unshift(newAuthor);
    };

    $scope.saveAuthor = function (author) {
        author.firstName = author.firstName.replace(/ + /g, ' ');
        author.lastName = author.lastName.replace(/ + /g, ' ');
        author.middleName = author.middleName.replace(/ + /g, ' ');
        author.books = author.books.filter(function(book) {
            return book.id != null;
        });
        if (author.id == null) {
            $http.post('authors', author).then(function (response) {
                $scope.refresh();
            }, function (response) {
                var index = $scope.authors.indexOf(author);
                if (response.status === 409) {
                    $scope.authors[index].$error = response.data.message;
                } else if (response.status === 403) {
                    $scope.authors[index].$error = 'Forbidden';
                } else {
                    $scope.authors[index].$error = 'Server error';
                }
            });
        } else {
            $http.put('authors/'+author.id, author).then(function (response) {
                $scope.refresh();
            }, function (response) {
                var index = $scope.authors.indexOf(author);
                if (response.status === 409) {
                    $scope.authors[index].$error = response.data.message;
                } else if (response.status === 403) {
                    $scope.authors[index].$error = 'Forbidden';
                } else {
                    $scope.authors[index].$error = 'Server error';
                }
            });
        }
    };

    $scope.saveBook = function (book) {
        book.name = book.name.replace(/ + /g, ' ');
        book.isbn = book.isbn.replace(/[ \-]/g, '');
        book.isbn = parseInt(book.isbn, 10);
        book.authors = book.authors.filter(function(author) {
            return author.id != null;
        });
        if (book.id == null) {
            $http.post('books', book).then(function (response) {
                $scope.refresh();
            }, function (response) {
                var index = $scope.books.indexOf(book);
                if (response.status === 409) {
                    $scope.books[index].$error = response.data.message;
                } else if (response.status === 403) {
                    $scope.books[index].$error = 'Forbidden';
                } else {
                    $scope.books[index].$error = 'Server error';
                }
            });
        } else {
            $http.put('books/'+book.id, book).then(function (response) {
                $scope.refresh();
            }, function (response) {
                var index = $scope.books.indexOf(book);
                if (response.status === 409) {
                    $scope.books[index].$error = response.data.message;
                } else if (response.status === 403) {
                    $scope.books[index].$error = 'Forbidden';
                } else {
                    $scope.books[index].$error = 'Server error';
                }
            });
        }
    };

    $scope.deleteAuthor = function (author) {
        $http.delete('authors/'+author.id).then(function (response) {
            $scope.refresh();
        }, function (response) {
            var index = $scope.authors.indexOf(author);
            if (response.status === 403) {
                $scope.authors[index].$error = 'Forbidden';
            } else {
                $scope.authors[index].$error = 'Server error';
            }
        });
    };

    $scope.deleteBook = function (book) {
        $http.delete('books/'+book.id).then(function (response) {
            $scope.refresh();
        }, function (response) {
            var index = $scope.books.indexOf(book);
            if (response.status === 403) {
                $scope.books[index].$error = 'Forbidden';
            } else {
                $scope.books[index].$error = 'Server error';
            }
        });
    };

    $scope.refresh = function () {
        $scope.authorAddLock = false;
        $scope.bookAddLock = false;
        $scope.getAuthors();
        $scope.getBooks();
    };

    // $scope.img = 'balance';

    $scope.refresh();

    $scope.uploadBookImage = function (book) {
        if (book.img) {
            var postfix = book.img.name.substr(-5);
            var reader = new FileReader();
            reader.readAsDataURL(book.img);
            reader.onload = function(evt) {
                $http.put('books/'+book.id+'/image', {'imageData':evt.target.result, 'postfix': postfix}).then(function (response) {
                    book.img = undefined;
                    book.$errorImg = undefined;
                    book.imageFileName = response.data.imageFileName;
                }, function (response) {
                    if (response.status === 403) {
                        book.$errorImg = 'Forbidden';
                    } else {
                        book.$errorImg = 'Server error';
                    }
                });
            };
        }
    };
}