<?php

namespace Acme\ApiBundle\Normalizer;

use Acme\CoreDomain\Author\Author;
use Acme\CoreDomain\Book\Book;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

class BookNormalizer implements NormalizerInterface
{
    use SerializerAwareTrait;

    /** @var Book $object */
    public function normalize($object, $format = null, array $context = array())
    {
        $this->setSerializer($context['serializer']);

        return [
            'id' => $object->getId()->getValue(),
            'name' => $object->getName(),
            'year' => $object->getYear(),
            'isbn' => $object->getIsbn(),
            'pagesNumber' => $object->getPagesNumber(),
            'imageFileName' => $object->getImageFileName(),
            'authors' => $object->getAuthors()->map(function (Author $author) use ($format, $context) {
                if (empty($context['include_relations'])) {
                    return [
                        'id' => $author->getId()->getValue(),
                        '__toString' => $author->__toString(),
                    ];
                } else {
                    $context['include_relations'] = false;

                    return $this->serializer->normalize($author, $format, $context);
                }
            })->getValues(),
            '__toString' => $object->__toString(),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Book;
    }
}