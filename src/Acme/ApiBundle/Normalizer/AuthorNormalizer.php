<?php

namespace Acme\ApiBundle\Normalizer;

use Acme\CoreDomain\Author\Author;
use Acme\CoreDomain\Book\Book;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

class AuthorNormalizer implements NormalizerInterface
{
    use SerializerAwareTrait;

    /** @var Author $object */
    public function normalize($object, $format = null, array $context = array())
    {
        $this->setSerializer($context['serializer']);

        return [
            'id' => $object->getId()->getValue(),
            'firstName' => $object->getFirstName(),
            'lastName' => $object->getLastName(),
            'middleName' => $object->getMiddleName(),
            'books' => $object->getBooks()->map(function (Book $book) use ($format, $context) {
                if (empty($context['include_relations'])) {
                    return [
                        'id' => $book->getId()->getValue(),
                        '__toString' => $book->__toString(),
                    ];
                } else {
                    $context['include_relations'] = false;

                    return $this->serializer->normalize($book, $format, $context);
                }
            })->getValues(),
            '__toString' => $object->__toString(),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Author;
    }
}