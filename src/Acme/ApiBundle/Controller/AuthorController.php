<?php

namespace Acme\ApiBundle\Controller;

use Acme\ApiBundle\Api\AuthorApi;
use Acme\CoreDomain\Author\AuthorId;
use Acme\CoreDomain\Author\AuthorRepositoryException;
use Acme\CoreDomain\Author\AuthorValidationException;
use Acme\CoreDomain\Book\BookId;
use Acme\CoreDomain\Book\BookRepositoryException;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class AuthorController extends Controller implements ClassResourceInterface
{
    private $serializer;

    private $api;

    public function __construct(SerializerInterface $serializer, AuthorApi $api)
    {
        $this->serializer = $serializer;
        $this->api = $api;
    }

    public function cgetAction(Request $request)
    {
        $authors = $this->api->getAll();

        $responseJson = $this->serializer->serialize($authors, 'json', [
            'serializer' => $this->serializer,
            'include_relations' => true
        ]);

        return new Response($responseJson, Response::HTTP_OK);
    }

    public function getAction(Request $request, int $id)
    {
        $authorId = new AuthorId($id);

        try {
            $author = $this->api->get($authorId);
        } catch (AuthorRepositoryException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_NOT_FOUND);
        }

        $responseJson = $this->serializer->serialize($author, 'json', [
            'serializer' => $this->serializer,
            'include_relations' => true
        ]);

        return new Response($responseJson, Response::HTTP_OK);
    }

    public function postAction(Request $request)
    {
        $firstName = $request->get('firstName');
        $lastName = $request->get('lastName');
        $middleName = $request->get('middleName');
        $bookIds = [];
        foreach ($request->get('books') as $bookData) {
            $bookIds[] = new BookId($bookData['id']);
        }

        try {
            $author = $this->api->create($firstName, $lastName, $middleName, $bookIds);
        } catch (AuthorRepositoryException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_FORBIDDEN);
        } catch (BookRepositoryException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_FORBIDDEN);
        } catch (AuthorValidationException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_CONFLICT);
        }

        $responseJson = $this->serializer->serialize($author, 'json', [
            'serializer' => $this->serializer,
            'include_relations' => true
        ]);

        return new Response($responseJson, Response::HTTP_RESET_CONTENT);
    }

    public function putAction(Request $request, int $id)
    {
        $authorId = new AuthorId($id);
        $firstName = $request->get('firstName');
        $lastName = $request->get('lastName');
        $middleName = $request->get('middleName');
        $bookIds = [];
        foreach ($request->get('books') as $bookData) {
            $bookIds[] = new BookId($bookData['id']);
        }

        try {
            $author = $this->api->update($authorId, $firstName, $lastName, $middleName, $bookIds);
        } catch (AuthorRepositoryException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_FORBIDDEN);
        } catch (BookRepositoryException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_FORBIDDEN);
        } catch (AuthorValidationException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_CONFLICT);
        }

        $responseJson = $this->serializer->serialize($author, 'json', [
            'serializer' => $this->serializer,
            'include_relations' => true
        ]);

        return new Response($responseJson, Response::HTTP_RESET_CONTENT);
    }

    public function deleteAction(Request $request, int $id)
    {
        $authorId = new AuthorId($id);

        try {
            $this->api->remove($authorId);
        } catch (AuthorRepositoryException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_FORBIDDEN);
        }

        return new Response('', Response::HTTP_RESET_CONTENT);
    }
}