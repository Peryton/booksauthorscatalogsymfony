<?php

namespace Acme\ApiBundle\Controller;

use Acme\ApiBundle\Api\BookApi;
use Acme\CoreDomain\Author\AuthorId;
use Acme\CoreDomain\Author\AuthorRepositoryException;
use Acme\CoreDomain\Book\BookId;
use Acme\CoreDomain\Book\BookRepositoryException;
use Acme\CoreDomain\Book\BookValidationException;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class BookController extends Controller implements ClassResourceInterface
{
    private $serializer;

    private $api;

    public function __construct(SerializerInterface $serializer, BookApi $api)
    {
        $this->serializer = $serializer;
        $this->api = $api;
    }

    public function cgetAction(Request $request)
    {
        $books = $this->api->getAll();

        $responseJson = $this->serializer->serialize($books, 'json', [
            'serializer' => $this->serializer,
            'include_relations' => true
        ]);

        return new Response($responseJson, Response::HTTP_OK);
    }

    public function getAction(Request $request, int $id)
    {
        $bookId = new BookId($id);

        try {
            $book = $this->api->get($bookId);
        } catch (BookRepositoryException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_NOT_FOUND);
        }

        $responseJson = $this->serializer->serialize($book, 'json', [
            'serializer' => $this->serializer,
            'include_relations' => true
        ]);

        return new Response($responseJson, Response::HTTP_OK);
    }

    public function postAction(Request $request)
    {
        $name = $request->get('name');
        $year = $request->get('year');
        $isbn = $request->get('isbn');
        $pagesNumber = $request->get('pagesNumber');
        $authorIds = [];
        foreach ($request->get('authors') as $authorData) {
            $authorIds[] = new AuthorId($authorData['id']);
        }

        try {
            $book = $this->api->create($name, $year, $isbn, $pagesNumber, $authorIds);
        } catch (BookRepositoryException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_FORBIDDEN);
        } catch (AuthorRepositoryException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_FORBIDDEN);
        } catch (BookValidationException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_CONFLICT);
        }

        $responseJson = $this->serializer->serialize($book, 'json', [
            'serializer' => $this->serializer,
            'include_relations' => true
        ]);

        return new Response($responseJson, Response::HTTP_RESET_CONTENT);
    }

    public function putAction(Request $request, int $id)
    {
        $bookId = new BookId($id);
        $name = $request->get('name');
        $year = $request->get('year');
        $isbn = $request->get('isbn');
        $pagesNumber = $request->get('pagesNumber');
        $authorIds = [];
        foreach ($request->get('authors') as $authorData) {
            $authorIds[] = new AuthorId($authorData['id']);
        }

        try {
            $book = $this->api->update($bookId, $name, $year, $isbn, $pagesNumber, $authorIds);
        } catch (BookRepositoryException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_FORBIDDEN);
        } catch (AuthorRepositoryException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_FORBIDDEN);
        } catch (BookValidationException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_CONFLICT);
        }

        $responseJson = $this->serializer->serialize($book, 'json', [
            'serializer' => $this->serializer,
            'include_relations' => true
        ]);

        return new Response($responseJson, Response::HTTP_RESET_CONTENT);
    }

    public function deleteAction(Request $request, int $id)
    {
        $bookId = new BookId($id);

        try {
            $this->api->remove($bookId);
        } catch (BookRepositoryException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_FORBIDDEN);
        }

        return new Response('', Response::HTTP_RESET_CONTENT);
    }

    public function putImageAction(Request $request, int $id)
    {
        $bookId = new BookId($id);
        $imageData = $request->get('imageData');
        $imageData = substr($imageData, strpos($imageData, ',') + 1);
        $decodedData = base64_decode($imageData);
        $postfix = $request->get('postfix');

        try {
            $imageFileName = $this->api->uploadImage($bookId, $decodedData, $postfix);
        } catch (BookRepositoryException $e) {
            $responseJson = json_encode(['message' => $e->getMessage()]);
            return new Response($responseJson, Response::HTTP_FORBIDDEN);
        }

        if (!$imageFileName) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        return new Response(json_encode(['imageFileName' => $imageFileName]), Response::HTTP_OK);
    }
}