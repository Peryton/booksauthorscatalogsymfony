<?php

namespace Acme\ApiBundle\Api;

use Acme\CoreDomain\Author\Author;
use Acme\CoreDomain\Author\AuthorFactoryInterface;
use Acme\CoreDomain\Author\AuthorId;
use Acme\CoreDomain\Author\AuthorRepositoryException;
use Acme\CoreDomain\Author\AuthorRepositoryInterface;
use Acme\CoreDomain\Author\AuthorValidationException;
use Acme\CoreDomain\Book\BookId;
use Acme\CoreDomain\Book\BookRepositoryException;
use Acme\CoreDomain\Book\BookRepositoryInterface;

class AuthorApi
{
    private $factory;

    private $repository;

    private $bookRepository;

    public function __construct(
        AuthorFactoryInterface $factory,
        AuthorRepositoryInterface $repository,
        BookRepositoryInterface $bookRepository
    ) {
        $this->factory = $factory;
        $this->repository = $repository;
        $this->bookRepository = $bookRepository;
    }

    /** @throws AuthorRepositoryException */
    public function get(AuthorId $authorId): Author
    {
        return $this->repository->getById($authorId);
    }

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    /**
     * @throws AuthorRepositoryException
     * @throws BookRepositoryException
     * @throws AuthorValidationException
     */
    public function create($firstName, $lastName, $middleName, array $bookIds): Author
    {
        $author = $this->factory->create($firstName, $lastName, $middleName, $bookIds);

        if ($author->isValid() == false) {
            throw new AuthorValidationException();
        }

        $this->repository->add($author)->save();

        return $author;
    }

    /**
     * @throws AuthorRepositoryException
     * @throws BookRepositoryException
     * @throws AuthorValidationException
     */
    public function update(AuthorId $authorId, $firstName, $lastName, $middleName, array $bookIds): Author
    {
        $author = $this->get($authorId);

        $books = [];
        /** @var BookId $bookId */
        foreach ($bookIds as $bookId) {
            $books[] = $this->bookRepository->getById($bookId);
        }

        $author->setData($firstName, $lastName, $middleName, $books);

        if ($author->isValid() == false) {
            throw new AuthorValidationException();
        }

        $this->repository->save();

        return $author;
    }

    /** @throws AuthorRepositoryException */
    public function remove(AuthorId $authorId)
    {
        $author = $this->get($authorId);

        $this->repository->remove($author)->save();
    }
}