<?php

namespace Acme\ApiBundle\Api;

use Acme\CoreDomain\Author\AuthorId;
use Acme\CoreDomain\Author\AuthorRepositoryException;
use Acme\CoreDomain\Author\AuthorRepositoryInterface;
use Acme\CoreDomain\Book\Book;
use Acme\CoreDomain\Book\BookFactoryInterface;
use Acme\CoreDomain\Book\BookId;
use Acme\CoreDomain\Book\BookRepositoryException;
use Acme\CoreDomain\Book\BookRepositoryInterface;
use Acme\CoreDomain\Book\BookValidationException;

class BookApi
{
    private $factory;

    private $repository;

    private $authorRepository;

    private $imgDir = 'img/';

    public function __construct(
        BookFactoryInterface $factory,
        BookRepositoryInterface $repository,
        AuthorRepositoryInterface $authorRepository
    ) {
        $this->factory = $factory;
        $this->repository = $repository;
        $this->authorRepository = $authorRepository;
    }

    /** @throws BookRepositoryException */
    public function get(BookId $bookId): Book
    {
        return $this->repository->getById($bookId);
    }

    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    /**
     * @throws BookRepositoryException
     * @throws AuthorRepositoryException
     * @throws BookValidationException
     */
    public function create($name, $year, $isbn, $pagesNumber, array $authorIds): Book
    {
        $book = $this->factory->create($name, $year, $isbn, $pagesNumber, $authorIds);

        if ($book->isValid() == false) {
            throw new BookValidationException();
        }

        $this->repository->add($book)->save();

        return $book;
    }

    /**
     * @throws BookRepositoryException
     * @throws AuthorRepositoryException
     * @throws BookValidationException
     */
    public function update(BookId $bookId, $name, $year, $isbn, $pagesNumber, array $authorIds): Book
    {
        $book = $this->get($bookId);

        $authors = [];
        /** @var AuthorId $authorId */
        foreach ($authorIds as $authorId) {
            $authors[] = $this->authorRepository->getById($authorId);
        }

        $book->setData($name, $year, $isbn, $pagesNumber, $authors);

        if ($book->isValid() == false) {
            throw new BookValidationException();
        }

        $this->repository->save();

        return $book;
    }

    /** @throws BookRepositoryException */
    public function remove(BookId $bookId)
    {
        $book = $this->get($bookId);

        $oldImageFileName = $book->getImageFileName();

        $this->repository->remove($book)->save();

        if ($oldImageFileName) {
            unlink($this->imgDir.$oldImageFileName);
        }
    }

    /** @throws BookRepositoryException */
    public function uploadImage(BookId $bookId, $decodedData, $postfix)
    {
        $book = $this->repository->getById($bookId);

        $imageFileName = md5(microtime()).$postfix;
        while (file_exists($this->imgDir.$imageFileName)) {
            $imageFileName = md5(microtime()).$postfix;
        }

        if (!file_put_contents($this->imgDir.$imageFileName, $decodedData)) {
            return false;
        }

        $oldImageFileName = $book->getImageFileName();

        $book->setImageFileName($imageFileName);
        try {
            $this->repository->save();
        } catch (BookRepositoryException $e) {
            unlink($this->imgDir.$imageFileName);

            throw new BookRepositoryException($e->getMessage());
        }

        if ($oldImageFileName) {
            unlink($this->imgDir.$oldImageFileName);
        }

        return $imageFileName;
    }
}