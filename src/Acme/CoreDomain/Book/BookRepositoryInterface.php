<?php

namespace Acme\CoreDomain\Book;

interface BookRepositoryInterface
{
    /** @throws BookRepositoryException */
    public function getById(BookId $bookId): Book;

    public function getAll(): array;

    public function add(Book $book): BookRepositoryInterface;

    public function remove(Book $book): BookRepositoryInterface;

    /** @throws BookRepositoryException */
    public function save(): BookRepositoryInterface;
}