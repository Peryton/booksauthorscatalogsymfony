<?php

namespace Acme\CoreDomain\Book;

class BookId
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function equal(BookId $bookId): bool
    {
        return $this->value === $bookId->value;
    }
}