<?php

namespace Acme\CoreDomain\Book;

class BookValidationException extends \Exception
{
    protected $message = 'Validation failed';
}