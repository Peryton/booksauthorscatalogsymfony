<?php

namespace Acme\CoreDomain\Book;

use Acme\CoreDomain\Author\AuthorRepositoryException;

interface BookFactoryInterface
{
    /** @throws AuthorRepositoryException */
    public function create($name, $year, $isbn, $pagesNumber, array $authorIds): Book;
}