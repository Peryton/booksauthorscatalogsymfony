<?php

namespace Acme\CoreDomain\Book;

use Acme\CoreDomain\Author\Author;
use Acme\CoreDomain\AuthorBookRelation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Isbn\Isbn;

abstract class Book
{
    use AuthorBookRelation;

    protected $id;

    protected $name;

    protected $year;

    protected $isbn;

    protected $pagesNumber;

    protected $imageFileName;

    protected $authors;

    public function __construct()
    {
        $this->authors = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name.', '.$this->year.'.';
    }

    abstract public function getId(): BookId;

    public function getName()
    {
        return $this->name;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function getIsbn()
    {
        return $this->isbn;
    }

    public function getPagesNumber()
    {
        return $this->pagesNumber;
    }

    public function getImageFileName()
    {
        return $this->imageFileName;
    }

    public function setImageFileName($imageFileName): Book
    {
        $this->imageFileName = $imageFileName;

        return $this;
    }

    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    public function addAuthor(Author $author): Book
    {
        $this->getAuthors()->add($author);

        return $this;
    }

    public function removeAuthor(Author $author): Book
    {
        $this->getAuthors()->removeElement($author);

        return $this;
    }

    public function setData($name, $year, $isbn, $pagesNumber, array $authors): Book
    {
        $this->name        = $name;
        $this->year        = $year;
        $this->isbn        = $isbn;
        $this->pagesNumber = $pagesNumber;

        /** @var Author $author */
        foreach ($this->getAuthors() as $author) {
            if (in_array($author, $authors) == false) {
                $this->removeAuthorBookRelation($author, $this);
            }
        }

        foreach ($authors as $author) {
            $this->addAuthorBookRelation($author,$this);
        }

        return $this;
    }

    public function isValid()
    {
        $isbnService = new Isbn();

        return strlen($this->name) > 0
            && $this->year > 0
            && $isbnService->validation->isbn(strval($this->isbn))
            && $this->pagesNumber > 0;
    }
}