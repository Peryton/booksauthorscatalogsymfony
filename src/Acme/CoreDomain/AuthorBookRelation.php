<?php

namespace Acme\CoreDomain;

use Acme\CoreDomain\Author\Author;
use Acme\CoreDomain\Book\Book;

trait AuthorBookRelation
{
    private function addAuthorBookRelation(Author $author, Book $book)
    {
        if ($author->getBooks()->contains($book) == false) {
            $author->addBook($book);
        }
        if ($book->getAuthors()->contains($author) == false) {
            $book->addAuthor($author);
        }

        return $this;
    }

    private function removeAuthorBookRelation(Author $author, Book $book)
    {
        $author->removeBook($book);
        $book->removeAuthor($author);

        return $this;
    }
}