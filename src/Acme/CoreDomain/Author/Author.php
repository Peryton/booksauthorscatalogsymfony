<?php

namespace Acme\CoreDomain\Author;

use Acme\CoreDomain\AuthorBookRelation;
use Acme\CoreDomain\Book\Book;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

abstract class Author
{
    use AuthorBookRelation;

    protected $id;

    protected $firstName;

    protected $lastName;

    protected $middleName;

    protected $books;

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->lastName.' '
            .mb_substr($this->firstName, 0, 1).'.'
            .mb_substr($this->middleName, 0, 1).'.';
    }

    abstract public function getId(): AuthorId;

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getMiddleName()
    {
        return $this->middleName;
    }

    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(Book $book): Author
    {
        $this->getBooks()->add($book);

        return $this;
    }

    public function removeBook(Book $book): Author
    {
        $this->getBooks()->removeElement($book);

        return $this;
    }

    public function setData($firstName, $lastName, $middleName, array $books): Author
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->middleName = $middleName;

        /** @var Book $book */
        foreach ($this->getBooks() as $book) {
            if (in_array($book, $books) == false) {
                $this->removeAuthorBookRelation($this, $book);
            }
        }

        foreach ($books as $book) {
            $this->addAuthorBookRelation($this, $book);
        }

        return $this;
    }

    public function isValid()
    {
        $pattern = '/^[A-ZА-ЯЁ]{1}[A-ZА-ЯЁa-zа-яё]*([\-\s]{1}[A-ZА-ЯЁa-zа-яё]+)*$/u';
        return preg_match($pattern, $this->firstName)
            && preg_match($pattern, $this->lastName)
            && preg_match($pattern, $this->middleName);
    }
}