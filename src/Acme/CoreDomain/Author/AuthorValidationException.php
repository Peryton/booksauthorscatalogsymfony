<?php

namespace Acme\CoreDomain\Author;

class AuthorValidationException extends \Exception
{
    protected $message = 'Validation failed';
}