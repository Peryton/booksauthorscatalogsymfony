<?php

namespace Acme\CoreDomain\Author;

interface AuthorRepositoryInterface
{
    /** @throws AuthorRepositoryException */
    public function getById(AuthorId $authorId): Author;

    public function getAll(): array;

    public function add(Author $author): AuthorRepositoryInterface;

    public function remove(Author $author): AuthorRepositoryInterface;

    /** @throws AuthorRepositoryException */
    public function save(): AuthorRepositoryInterface;
}