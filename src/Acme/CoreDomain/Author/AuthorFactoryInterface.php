<?php

namespace Acme\CoreDomain\Author;

use Acme\CoreDomain\Book\BookRepositoryException;

interface AuthorFactoryInterface
{
    /** @throws BookRepositoryException */
    public function create($firstName, $lastName, $middleName, array $bookIds): Author;
}