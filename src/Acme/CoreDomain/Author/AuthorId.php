<?php

namespace Acme\CoreDomain\Author;

class AuthorId
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function equal(AuthorId $authorId): bool
    {
        return $this->value === $authorId->value;
    }
}