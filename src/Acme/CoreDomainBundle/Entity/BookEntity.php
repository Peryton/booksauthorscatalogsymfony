<?php

namespace Acme\CoreDomainBundle\Entity;

use Acme\CoreDomain\Book\Book;
use Acme\CoreDomain\Book\BookId;

class BookEntity extends Book
{
    public function getId(): BookId
    {
        return new BookId($this->id);
    }
}