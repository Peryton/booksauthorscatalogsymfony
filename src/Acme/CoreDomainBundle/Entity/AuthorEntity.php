<?php

namespace Acme\CoreDomainBundle\Entity;

use Acme\CoreDomain\Author\Author;
use Acme\CoreDomain\Author\AuthorId;

class AuthorEntity extends Author
{
    public function getId(): AuthorId
    {
        return new AuthorId($this->id);
    }
}