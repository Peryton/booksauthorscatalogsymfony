<?php

namespace Acme\CoreDomainBundle\Repository;

use Acme\CoreDomain\Book\Book;
use Acme\CoreDomain\Book\BookId;
use Acme\CoreDomain\Book\BookRepositoryException;
use Acme\CoreDomain\Book\BookRepositoryInterface;
use Doctrine\ORM\EntityRepository;

class BookRepository extends EntityRepository implements BookRepositoryInterface
{
    /** @throws BookRepositoryException */
    public function getById(BookId $bookId): Book
    {
        /** @var Book $book */
        $book = $this->find($bookId->getValue());

        if ($book == null) {
            throw new BookRepositoryException('Book with id:'.$bookId->getValue().' can not be found.');
        }

        return $book;
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function add(Book $book): BookRepositoryInterface
    {
        $this->_em->persist($book);

        return $this;
    }

    public function remove(Book $book): BookRepositoryInterface
    {
        $this->_em->remove($book);

        return $this;
    }

    /** @throws BookRepositoryException */
    public function save(): BookRepositoryInterface
    {
        try {
            $this->_em->flush();
        } catch (\Exception $e) {
            throw new BookRepositoryException($e->getMessage());
        }

        return $this;
    }
}