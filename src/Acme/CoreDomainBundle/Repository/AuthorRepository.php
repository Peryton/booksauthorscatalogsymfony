<?php

namespace Acme\CoreDomainBundle\Repository;

use Acme\CoreDomain\Author\Author;
use Acme\CoreDomain\Author\AuthorId;
use Acme\CoreDomain\Author\AuthorRepositoryException;
use Acme\CoreDomain\Author\AuthorRepositoryInterface;
use Doctrine\ORM\EntityRepository;

class AuthorRepository extends EntityRepository implements AuthorRepositoryInterface
{
    /** @throws AuthorRepositoryException */
    public function getById(AuthorId $authorId): Author
    {
        /** @var Author $author */
        $author = $this->find($authorId->getValue());

        if ($author == null) {
            throw new AuthorRepositoryException('Author with id:'.$authorId->getValue().' can not be found.');
        }

        return $author;
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function add(Author $author): AuthorRepositoryInterface
    {
        $this->_em->persist($author);

        return $this;
    }

    public function remove(Author $author): AuthorRepositoryInterface
    {
        $this->_em->remove($author);

        return $this;
    }

    /** @throws AuthorRepositoryException */
    public function save(): AuthorRepositoryInterface
    {
        try {
            $this->_em->flush();
        } catch (\Exception $e) {
            throw new AuthorRepositoryException($e->getMessage());
        }

        return $this;
    }
}