<?php

namespace Acme\CoreDomainBundle\Factory;

use Acme\CoreDomain\Author\AuthorRepositoryException;
use Acme\CoreDomain\Author\AuthorRepositoryInterface;
use Acme\CoreDomain\Book\Book;
use Acme\CoreDomain\Book\BookFactoryInterface;
use Acme\CoreDomainBundle\Entity\BookEntity;

class BookFactory implements BookFactoryInterface
{
    private $authorRepository;

    public function __construct(AuthorRepositoryInterface $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    /** @throws AuthorRepositoryException */
    public function create($name, $year, $isbn, $pagesNumber, array $authorIds): Book
    {
        $book = new BookEntity();

        $authors = [];
        foreach ($authorIds as $authorId) {
            $authors[] = $this->authorRepository->getById($authorId);
        }

        $book->setData($name, $year, $isbn, $pagesNumber, $authors);

        return $book;
    }
}