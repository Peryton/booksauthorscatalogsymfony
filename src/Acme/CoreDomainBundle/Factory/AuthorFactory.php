<?php

namespace Acme\CoreDomainBundle\Factory;

use Acme\CoreDomain\Author\Author;
use Acme\CoreDomain\Author\AuthorFactoryInterface;
use Acme\CoreDomain\Book\BookRepositoryException;
use Acme\CoreDomain\Book\BookRepositoryInterface;
use Acme\CoreDomainBundle\Entity\AuthorEntity;

class AuthorFactory implements AuthorFactoryInterface
{
    private $bookRepository;

    public function __construct(BookRepositoryInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    /** @throws BookRepositoryException */
    public function create($firstName, $lastName, $middleName, array $bookIds): Author
    {
        $author = new AuthorEntity();

        $books = [];
        foreach ($bookIds as $bookId) {
            $books[] = $this->bookRepository->getById($bookId);
        }

        $author->setData($firstName, $lastName, $middleName, $books);

        return $author;
    }
}